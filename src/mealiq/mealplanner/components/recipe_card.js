import React from "react";
import Card from 'react-bootstrap/Card';
import classNames from "classnames";
import {Nutrition} from "./nutrition";

const RecipeCard = ({ recipe, day, mealType }) =>  {
    const onShowRecipeDetails = () => dispatch({ type: 'RECIPE_DETAILS_SHOW', day, mealType });

    if(!recipe) return null;
    return <div className={classNames("recipe-card", "row")}
                onClick={onShowRecipeDetails}>
        <div className="col-4">
            <figure style={{backgroundImage: `url(${recipe.image_url})`}}></figure>
        </div>
        <div className="col-8">
            <Card.Body>
                <Card.Title>{recipe.title}</Card.Title>
                <Card.Text>
                    <p className="short-description">{recipe.short_description}</p>
                    <p className="short-nutrition">
                        <Nutrition metric="calories" quantity={recipe.nutrition_per_serving.calories.quantity} />
                    </p>
                </Card.Text>
            </Card.Body>
        </div>
    </div>;
};

export default RecipeCard;
