import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {recipeOptionsShow} from "../services/operations";

const MealControls = ({ mealType }) => {
    const today = useSelector(state => state.today);
    const day = useSelector((state) => state.diaryDayVisible);

    const dispatch = useDispatch();

    const onRecipeOptionsShow = () => {
        dispatch(recipeOptionsShow(today, day, mealType));
    };

    return <div className="recipe-controls row">
        <div className="col-3"></div>
        <div className="col-3"></div>
        <div className="col-3"></div>
        <div className="col-3"><a href="#" onClick={onRecipeOptionsShow}>Change</a></div>
    </div>;
};

export default MealControls;
