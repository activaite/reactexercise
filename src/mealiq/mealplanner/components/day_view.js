import React from "react";
import {useSelector} from "react-redux";
import RecipeCard from "./recipe_card";
import MealControls from "./meal_controls";

const cardContents = (plannedMeal) => {
    return <RecipeCard day={plannedMeal.date} mealType={plannedMeal.meal_type} recipe={plannedMeal.recipe}/>
};

const DayView = ({ dayPlannedMeals }) => {
    const planMealTypes = useSelector((state) => state.planMealTypes);

    return <div className="day-view">
        {planMealTypes.map(mealType => {
            let plannedMeal = dayPlannedMeals.find(pm => pm.meal_type === mealType);
            return <div className="day-meal" key={"day-meal-" + plannedMeal.date + "-" + plannedMeal.meal_type}>
                <div>{mealType}</div>
                {cardContents(plannedMeal)}
                <MealControls mealType={mealType} />
            </div>
        })}
        <br/><br/><br/>
    </div>;
};

export default DayView;
