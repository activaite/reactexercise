import React from "react";
import Button from "react-bootstrap/Button";
import { useDispatch, useSelector } from 'react-redux';

const dayLabel = (dayIndex) => "Day " + (dayIndex + 1);

const DiaryNav = () => {
    const planDays = useSelector(state => state.planDays);
    const diaryDayVisible = useSelector(state => state.diaryDayVisible);

    const dispatch = useDispatch();

    const onNavigateToDay = (day) => {
        dispatch({type: 'CHANGE_DIARY_DAY_VISIBLE', day});
    };

    let dayIndex = planDays.indexOf(diaryDayVisible);
    let prevText = "";
    let nextText = "";
    let currentText = dayLabel(dayIndex);
    if(dayIndex > 0) {
        let prevIndex = dayIndex - 1;
        let prevDay = planDays[prevIndex];
        prevText = <Button onClick={() => onNavigateToDay(prevDay)}>&laquo;</Button>;
    }
    if(dayIndex < planDays.length - 1) {
        let nextIndex = dayIndex + 1;
        let nextDay = planDays[nextIndex];
        nextText = <Button onClick={() => onNavigateToDay(nextDay)}>&raquo;</Button>;
    }
    return <div className="diary-nav row">
        <div className="col-4">{prevText}</div>
        <div className="col-4">{currentText}</div>
        <div className="col-4">{nextText}</div>
    </div>;
};

export default DiaryNav;
