import React, { useEffect } from "react";
import {useDispatch, useSelector} from "react-redux";
import Diary from "./diary";
import {appLoad} from "../services/operations";

const App = () => {
    const today = useSelector(state => state.today);
    const planDays = useSelector(state => state.planDays);
    const plannedMeals = useSelector(state => state.plannedMeals);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(appLoad(today, planDays));
    }, [today, planDays, dispatch]);

    return <Diary plannedMeals={plannedMeals} />;
}

export default App;
