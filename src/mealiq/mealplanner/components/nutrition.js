import React from "react";

const labelsForMetric = {
    calories: "Calories",
    fat: "Fat",
    saturates: "Saturates",
    sugars: "Sugar",
    protein: "Protein",
    carbohydrate: "Carbs",
    fibre: "Fibre",
    salt: "Salt"
}

const unitForMetric = (metricName) => metricName === "calories" ? "kCal" : " g";

export const Nutrition = ({ metric, quantity }) => <>
    <span className="metric">{labelsForMetric[metric]} </span> <b className="quantity">{quantity.toFixed(2)}{unitForMetric(metric)}</b>
</>