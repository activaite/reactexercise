import React from "react";
import DayView from "./day_view";
import DiaryNav from "./diary_nav";
import Button from "react-bootstrap/Button";
import {useSelector} from "react-redux";

const Diary = ({ plannedMeals }) => {
    const diaryDayVisible = useSelector(state => state.diaryDayVisible);
    const plannedMealsNetworkState = useSelector(state => state.plannedMealsNetworkState);
    const dayPlannedMeals = plannedMeals.filter(pm => pm.date === diaryDayVisible);
    console.log(dayPlannedMeals);
    return <>
        <div className="sticky-top diary-header">
            <div className="float-right">
                <Button variant="secondary">Prefs</Button>
            </div>
            <h2>My Diary</h2>
            <DiaryNav />
        </div>
        {plannedMealsNetworkState.loading ? "Loading meal plans..." : <DayView dayPlannedMeals={dayPlannedMeals} />}
    </>
};

export default Diary;
