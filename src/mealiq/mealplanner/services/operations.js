import {fetchDayPlan} from "./api_calls";
import {preloadedState} from "./mock_data";

const plannedMealsFetch = (plannedMeals, estimatedCost, plannedMealsNetworkState) =>
    ({ type: 'PLANNED_MEALS_FETCH', plannedMeals, estimatedCost, plannedMealsNetworkState});

export const appLoad = (today, planDays) => {
    return (dispatch) => {
        dispatch(plannedMealsFetch([], 0, {loading: true}));
        fetchDayPlan(today, planDays) // fetchCurrentPlannedMeals(planDays)
            .then(response => {
                // if(response.ok) {
                    response.json().then(jsonBody => {
                        dispatch(plannedMealsFetch(jsonBody.plans, jsonBody.cost, {loaded: true, error: null}));
                    })
                // } else {
                //     response.text().then(errorText => dispatch(plannedMealsFetch(preloadedState.plannedMeals, 0.01, {loaded: false, error: "Response not OK: " + errorText})));
                // }
            })
            .catch(reason => dispatch(plannedMealsFetch(preloadedState.plannedMeals, 0.01, {loaded: false, error: "Caught: " + reason.toString()})));
    }
};

export const recipeOptionsShow = (today, day, mealType) => {
    // TODO implement me
};