const mergePlannedMeals = (originalPlannedMeals, newPlannedMeals) => {
    let unchangedPlannedMeals = originalPlannedMeals.filter(pm => !newPlannedMeals.some(npm => pm.date === npm.date && pm.meal_type === npm.meal_type));
    return unchangedPlannedMeals.concat(newPlannedMeals);
}

const diaryReducer = (state, action) => {
    switch (action.type) {
        case 'PLANNED_MEALS_FETCH': {
            return {...state,
                plannedMeals: mergePlannedMeals(state.plannedMeals, action.plannedMeals),
                estimatedCost: action.estimatedCost,
                plannedMealsNetworkState: action.plannedMealsNetworkState
            }
        }
        case 'CHANGE_DIARY_DAY_VISIBLE': {
            return {...state, diaryDayVisible: action.day};
        }
        default:
            return state
    }
}

export default diaryReducer
